Malayalam conjuncts in GNU Freefont
===================================

This is a list of Malayalam conjuncts found in the development version of GNU Freefont. Which also includes the conjuncts found in ancient Sanskrit documents. A lot of conjuncts are taken from [Rachana](https://smc.org.in/fonts/rachana) and [Meera](https://smc.org.in/fonts/meera) fonts. The evidence_of_conjuncts folder has printed version of conjuncts that are newly added by Freefont. Note that from a printed version of a conjunct, for example ങ്ഗ,  we have ങ്ഗ, ങ്ഗു and ങ്ഗൂ.

Also note that this list is a **subset** of the conjuncts that can be formed by the font and this is a work in progress. 

For more details on the font: https://www.gnu.org/software/freefont/

